<?php

namespace PhpGangsters\EasyContent\HtmlHandler\DataTypes;

final class Str
{
    public static function removeExtraWhitespaces($str)
    {
        $processedString = mb_ereg_replace("[ \t\n\r\0\x0B]+", ' ', $str);
        return trim($processedString);
    }
}
