<?php

namespace PhpGangsters\EasyContent\HtmlHandler\Tests\Unit;

use PhpGangsters\EasyContent\HtmlHandler\File;
use PhpGangsters\EasyContent\HtmlHandler\FileList;
use PHPUnit\Framework\TestCase;

class FileListTest extends TestCase
{
    /** @var FileList $fileList */
    protected $fileList;


    public function setUp()
    {
        $this->fileList = new FileList();

        $this->fileList->add(
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg',
            new File('https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg')
        );

        $this->fileList->add(
            'https://ec-local-attachments.s3.amazonaws.com/1/25/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg',
            new File('https://ec-local-attachments.s3.amazonaws.com/1/25/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg')
        );

        $this->fileList->addFile(new File('https://ec-local-attachments.s3.amazonaws.com/1/22/39/azaza/jpg.jpg'));
    }


    public function testGetsFileNamesArrayProperly()
    {
        $this->assertEquals([
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg' => '1_22_39_-txlt3wpvwg1v7lqikalk4qzhnxhy11l_jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/25/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg' => '1_25_39_-txlt3wpvwg1v7lqikalk4qzhnxhy11l_jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/azaza/jpg.jpg' => '1_22_39_azaza_jpg.jpg'
        ], $this->fileList->getFileNames());
    }


    public function testEmptyWorksProperly()
    {
        $emptyFileList = new FileList();

        $this->assertFalse($this->fileList->isEmpty());
        $this->assertTrue($emptyFileList->isEmpty());
    }


    public function testHasMethodWorksProperly()
    {
        $fileInList  = 'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg';
        $missingFile = 'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/not-found.jpg';

        $this->assertTrue($this->fileList->has($fileInList));
        $this->assertFalse($this->fileList->has($missingFile));
    }


    public function testGetMethodWorksProperly()
    {
        $fileInList  = 'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg';
        $missingFile = 'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/not-found.jpg';

        // TODO:
        $this->assertNull($this->fileList->get($missingFile));
    }


    public function testAddsFilesFromArrayProperly()
    {
        $this->fileList->addFilesFromArray([
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/azaza/jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/not-found.jpg'
        ]);

        $this->assertEquals([
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg' => '1_22_39_-txlt3wpvwg1v7lqikalk4qzhnxhy11l_jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/25/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg' => '1_25_39_-txlt3wpvwg1v7lqikalk4qzhnxhy11l_jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/azaza/jpg.jpg' => '1_22_39_azaza_jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/not-found.jpg' => '1_22_39_-txlt3wpvwg1v7lqikalk4qzhnxhy11l_not-found.jpg'
        ], $this->fileList->getFileNames());
    }


    public function testCanBeMergedProperly()
    {
        $fl = new FileList();

        $fl->addFilesFromArray([
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/azaza/jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/not-found.jpg'
        ]);

        $this->fileList->merge($fl);

        $this->assertEquals([
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg' => '1_22_39_-txlt3wpvwg1v7lqikalk4qzhnxhy11l_jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/25/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg' => '1_25_39_-txlt3wpvwg1v7lqikalk4qzhnxhy11l_jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/azaza/jpg.jpg' => '1_22_39_azaza_jpg.jpg',
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/not-found.jpg' => '1_22_39_-txlt3wpvwg1v7lqikalk4qzhnxhy11l_not-found.jpg'
        ], $this->fileList->getFileNames());
    }
}
