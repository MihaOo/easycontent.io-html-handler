<?php

namespace PhpGangsters\EasyContent\HtmlHandler\Tests\Unit\DataTypes;

use PhpGangsters\EasyContent\HtmlHandler\DataTypes\Url;
use PHPUnit\Framework\TestCase;

class UrlTest extends TestCase
{
    public function testValuableExtensionsArrayGetterReturnsProperArray()
    {
        $imagesUrlObject = new Url('http://google.com/lol.png', Url::IMAGES_URL);
        $combinedUrlObject = new Url('http://google.com/lol.png', Url::IMAGES_URL | Url::AUDIO_URL);

        $this->assertEquals(['jpg', 'jpeg', 'png', 'gif', 'ico'], $imagesUrlObject->getValuableExtensionsArray());
        $this->assertEquals(
            ['jpg', 'jpeg', 'png', 'gif', 'ico', 'mp3', 'm4a', 'ogg', 'wav'],
            $combinedUrlObject->getValuableExtensionsArray()
        );
    }


    public function testGeneratesValidRegularExpresion()
    {
        $imagesUrlObject = new Url('http://google.com/lol.png', Url::IMAGES_URL);
        $combinedUrlObject = new Url('http://google.com/lol.png', Url::IMAGES_URL | Url::AUDIO_URL);

        $regex = $imagesUrlObject->getRegularExpression('google.com');
        $complexRegex = $combinedUrlObject->getRegularExpression('s3.aws.com');

        $expectedRegex = 'https?:\/\/google\.com.*(jpg|jpeg|png|gif|ico)';
        $expectedComplex = 'https?:\/\/s3\.aws\.com.*(jpg|jpeg|png|gif|ico|mp3|m4a|ogg|wav)';


        $this->assertEquals($expectedRegex, $regex);
        $this->assertEquals($expectedComplex, $complexRegex);
    }


    public function testUrlMatchesRegularExpresion()
    {
        $imagesUrlObject = new Url(
            'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg',
            Url::IMAGES_URL
        );

        $result = $imagesUrlObject->matchesRegularExpression('ec-local-attachments.s3.amazonaws.com');
        $badResult = $imagesUrlObject->matchesRegularExpression('google.com');

        $this->assertTrue($result);
        $this->assertFalse($badResult);
    }
}
