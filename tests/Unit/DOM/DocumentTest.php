<?php

namespace PhpGangsters\EasyContent\HtmlHandler\Tests\Unit\DOM;

use PhpGangsters\EasyContent\HtmlHandler\DataTypes\Url;
use PhpGangsters\EasyContent\HtmlHandler\DOM\Document;
use PhpGangsters\EasyContent\HtmlHandler\DOM\Helper;
use PhpGangsters\EasyContent\HtmlHandler\FileList;
use PHPUnit\Framework\TestCase;

class DocumentTest extends TestCase
{
    /** @var string $html */
    protected $html = '<html><body><span id="node-to-delete">Привет, Мир!</span><p><span class="long list of annotation classes">This is annotation</span></p></body></html>';

    /** @var null|Document $document */
    protected $document;

    /** @var null|bool $loadingResult */
    protected $loadingResult;


    public function setUp()
    {
        $this->document = new Document('', '', Url::LOCAL_BASE);
        $this->loadingResult = $this->document->loadHTML($this->html);
    }


    public function testLoadsValidHtmlWithoutErrors()
    {
        $this->assertEquals(true, $this->loadingResult);
        $this->assertEmpty($this->document->getLoadingErrors());
    }


    public function testSavesHtmlProperly()
    {
        $this->assertEquals($this->html, $this->document->saveHTML());
    }


    public function testProperlyDeletesOuterHtml()
    {
        $spanToDelete = $this->document->getElementById('node-to-delete');
        Helper::removeChildOuter($spanToDelete);

        $this->assertEquals(
            '<html><body>Привет, Мир!<p><span class="long list of annotation classes">This is annotation</span></p></body></html>',
            $this->document->saveHTML()
        );
    }


    public function testRemovesAnnotationsProperly()
    {
        $this->document->removeAnnotations();

        $this->assertEquals(
            '<html><body><span id="node-to-delete">Привет, Мир!</span><p>This is annotation</p></body></html>',
            $this->document->saveHTML()
        );
    }


    public function testFilesListEmptyWhenNoFileLinksWasFound()
    {
        $this->document->parse();

        $this->assertTrue($this->document->getFilesList()->isEmpty());
    }


    public function testNodesPropertyIsImmutable()
    {
        $nodes = $this->document->getNodes();
        $nodes = ['a', 'b', 'c'];

        $this->assertEmpty($this->document->getNodes());
    }


    public function testFilesListPropertyIsImmutable()
    {
        $filesList = $this->document->getFilesList();
        $filesList = null;

        $this->assertInstanceOf(FileList::class, $this->document->getFilesList());
    }


    public function testParserFindsAllDocuments()
    {
        $html = <<<'EOD'
<html>
    <body>
        <span id="node-to-delete">Привет, Мир!</span>
        <p>
            <span class="long list of annotation classes">This is annotation</span>
            <img src="https://ec-local-attachments.s3.amazonaws.com/image.png">
            <img alt="https://ec-local-attachments.s3.amazonaws.com/image_2.png">
        </p>
        <div>
            <a href="http://google.com">First link</a>
            <a href="https://ec-local-attachments.s3.amazonaws.com/image.png">Second link</a>
        </div>
        <section>
            <p>This is plain text with url https://ec-local-attachments.s3.amazonaws.com/image.png</p>
            <video src="https://ec-local-attachments.s3.amazonaws.com/video.avi"></video>            
        </section>
    </body>
</html>
EOD;

        $document = new Document('', '', Url::LOCAL_BASE);

        $document->loadHTML($html);
        $document->removeAnnotations();
        $document->parse();

        $filesList = $document->getFilesList();

        $this->assertEquals(
            [
                'https://ec-local-attachments.s3.amazonaws.com/image.png',
                'https://ec-local-attachments.s3.amazonaws.com/video.avi'
            ],
            array_keys($filesList->getItems())
        );
    }


    public function testStringToHtmlConversion()
    {
        $document = new Document('', '', Url::LOCAL_BASE);

        $document->loadHTML('Lorem ipsum dolor sit amet');
        $document->removeAnnotations();
        $document->parse();

        $this->assertEmpty($document->getLoadingErrors());

        $this->assertTrue($document->getFilesList()->isEmpty());

        $this->assertEquals(
            '<p>Lorem ipsum dolor sit amet</p>',
            $document->saveHTML()
        );
    }
}
