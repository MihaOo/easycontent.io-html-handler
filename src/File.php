<?php

namespace PhpGangsters\EasyContent\HtmlHandler;

final class File
{
    /** @var string $s3Url */
    private $s3Url;

    /** @var null|string $wpUrl */
    private $wpUrl;

    public function __construct($s3Url)
    {
        $this->s3Url = $s3Url;
    }

    public function getS3Url()
    {
        return $this->s3Url;
    }

    public function getWPUrl()
    {
        return $this->wpUrl;
    }

    public function setWPUrl($url)
    {
        $this->wpUrl = $url;
    }

    public static function getFileNameFromS3Url($url)
    {
        // e.g. https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg

        $parts = explode('/', $url);
        $nameParts = array_slice($parts, -5);

        return implode('_', $nameParts);
    }
}
