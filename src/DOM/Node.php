<?php

namespace PhpGangsters\EasyContent\HtmlHandler\DOM;

final class Node
{
    /** @var \DOMElement $sourceNode */
    private $sourceNode;

    /** @var NodeAttributeList $attributes */
    private $attributes;

    public function __construct(\DOMElement $node)
    {
        $this->sourceNode = $node;
        $this->attributes = new NodeAttributeList();
    }

    public function addAttribute(NodeAttribute $attribute)
    {
        $this->attributes->addAttribute($attribute);
    }

    public function isAttributesListEmpty()
    {
        return $this->attributes->isEmpty();
    }

    public function getSourceNode()
    {
        return $this->sourceNode;
    }

    public function getAttributeList()
    {
        return $this->attributes;
    }
}
