<?php

namespace PhpGangsters\EasyContent\HtmlHandler;

final class FileList implements \IteratorAggregate, \Countable, \ArrayAccess
{
    /** @var File[] $items */
    private $items = [];

    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public function count()
    {
        return count($this->items);
    }

    public function offsetExists($url)
    {
        return $this->has($url);
    }

    public function offsetGet($url)
    {
        return $this->get($url);
    }

    public function offsetSet($url, $file)
    {
        $this->add($url, $file);
    }

    public function offsetUnset($url) { }

    public function has($url)
    {
        return isset($this->items[$url]);
    }

    public function get($url)
    {
        return $this->has($url) ? $this->items[$url] : null;
    }

    public function add($url, $file)
    {
        if ($this->has($url)) {
            return;
        }

        $this->items[$url] = $file;
    }

    /**
     * @param File $file
     */
    public function addFile($file)
    {
        $this->add($file->getS3Url(), $file);
    }

    public function getFileNames()
    {
        $keys = array_keys($this->items);
        $values = array_map([File::class, 'getFileNameFromS3Url'], $keys);

        return array_combine($keys, $values);
    }

    public function isEmpty()
    {
        return empty($this->items);
    }

    public function addFilesFromArray(array $urls)
    {
        foreach ($urls as $url) {
            $this->addFile(new File($url));
        }
    }

    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param static $filesList
     */
    public function merge($filesList)
    {
        /**
         * @var string $url
         * @var File $file
         */
        foreach ($filesList->getItems() as $url => $file) {
            $this->addFile($file);
        }
    }
}
