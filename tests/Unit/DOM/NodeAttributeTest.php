<?php

namespace PhpGangsters\EasyContent\HtmlHandler\Tests\Unit\DOM;

use PhpGangsters\EasyContent\HtmlHandler\DataTypes\Url;
use PhpGangsters\EasyContent\HtmlHandler\DOM\NodeAttribute;
use PHPUnit\Framework\TestCase;

class NodeAttributeTest extends TestCase
{
    /**
     * @dataProvider parsingDataProvider
     *
     * @param string $name
     * @param string $value
     * @param integer $type
     * @param array|string[] $expectedArray
     */
    public function testParseMethodSucceeded($name, $value, $type, $expectedArray)
    {
        $attribute = new NodeAttribute($name, $value, $type);
        $fileList = $attribute->parse(Url::LOCAL_BASE);

        $this->assertEquals($expectedArray, array_keys($fileList->getItems()));
    }


    public function parsingDataProvider()
    {
        return [
            [
                NodeAttribute::SRC,
                'https://ec-local-attachments.s3.amazonaws.com/image.png',
                Url::IMAGES_URL,
                ['https://ec-local-attachments.s3.amazonaws.com/image.png']
            ],
            [
                NodeAttribute::HREF,
                'https://ec-local-attachments.s3.amazonaws.com/video.avi',
                Url::IMAGES_URL | Url::AUDIO_URL,
                []
            ],
            [
                NodeAttribute::SRC,
                'https://ec-local-attachments.s3.amazonaws.com/music.mp3',
                Url::IMAGES_URL,
                []
            ],
            [
                NodeAttribute::HREF,
                'https://ec-local-attachments.s3.amazonaws.com/music.mp3',
                Url::IMAGES_URL | Url::AUDIO_URL,
                ['https://ec-local-attachments.s3.amazonaws.com/music.mp3']
            ],
            [
                NodeAttribute::SRC,
                'https://google.com/image.gif',
                Url::IMAGES_URL,
                []
            ],
            [
                NodeAttribute::HREF,
                'https://google.com/document.docx',
                Url::IMAGES_URL | Url::AUDIO_URL,
                []
            ],
            [
                NodeAttribute::SRCSET,
                'https://ec-local-attachments.s3.amazonaws.com/image_1x.png 1x, https://ec-local-attachments.s3.amazonaws.com/image_2x.png 2x',
                Url::IMAGES_URL,
                [
                    'https://ec-local-attachments.s3.amazonaws.com/image_1x.png',
                    'https://ec-local-attachments.s3.amazonaws.com/image_2x.png'
                ]
            ],
            [
                NodeAttribute::SRCSET,
                'https://ec-local-attachments.s3.amazonaws.com/image_1.png, https://ec-local-attachments.s3.amazonaws.com/image_2.png',
                Url::IMAGES_URL,
                [
                    'https://ec-local-attachments.s3.amazonaws.com/image_1.png',
                    'https://ec-local-attachments.s3.amazonaws.com/image_2.png'
                ]
            ],
            [
                NodeAttribute::SRCSET,
                'https://ec-local-attachments.s3.amazonaws.com/image.png',
                Url::IMAGES_URL,
                ['https://ec-local-attachments.s3.amazonaws.com/image.png']
            ],
            [
                NodeAttribute::SRCSET,
                'https://ec-local-attachments.s3.amazonaws.com/image.png 1x, https://ec-local-attachments.s3.amazonaws.com/image.png',
                Url::IMAGES_URL,
                ['https://ec-local-attachments.s3.amazonaws.com/image.png']
            ],
            [
                NodeAttribute::SRCSET,
                'https://ec-local-attachments.s3.amazonaws.com/image_1x.png 1x, https://ec-local-attachments.s3.amazonaws.com/image_300w.png 300w',
                Url::IMAGES_URL,
                [
                    'https://ec-local-attachments.s3.amazonaws.com/image_1x.png',
                    'https://ec-local-attachments.s3.amazonaws.com/image_300w.png'
                ]
            ],
            [
                NodeAttribute::SRCSET,
                'https://google.com/image_1x.png 1x, https://google.com/image_2x.png 2x',
                Url::IMAGES_URL,
                []
            ],
            [
                NodeAttribute::SRCSET,
                'https://google.com/image_1x.png 1x, https://ec-local-attachments.s3.amazonaws.com/image_300w.png 2x',
                Url::IMAGES_URL,
                ['https://ec-local-attachments.s3.amazonaws.com/image_300w.png']
            ]
        ];
    }
}
