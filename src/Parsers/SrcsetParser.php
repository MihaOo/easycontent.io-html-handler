<?php

namespace PhpGangsters\EasyContent\HtmlHandler\Parsers;

use PhpGangsters\EasyContent\HtmlHandler\DataTypes\Str;
use PhpGangsters\EasyContent\HtmlHandler\DataTypes\Url;
use PhpGangsters\EasyContent\HtmlHandler\DOM\NodeAttribute;

final class SrcsetParser extends BaseParser
{
    /**
     * @param NodeAttribute $nodeAttribute
     * @return array|string[]
     */
    public function parse(NodeAttribute $nodeAttribute)
    {
        $urlRegex = (new Url('', $nodeAttribute->getExpectedUrlType()))
            ->getRegularExpression($this->s3UrlBase);

        $pattern = "^ ?({$urlRegex})( [1-9][0-9]*(w|x))?$";
        $normalizedAttributeValue = Str::removeExtraWhitespaces($nodeAttribute->getValue());

        $urls = [];
        $values = explode(',', $normalizedAttributeValue);

        foreach ($values as $value) {
            $matches = [];
            $result = mb_ereg($pattern, $value, $matches);

            if (false === $result || !$matches) {
                continue;
            }

            $urls[] = $matches[1];
        }

        return array_unique($urls);
    }
}
