<?php

namespace PhpGangsters\EasyContent\HtmlHandler\DataTypes;

final class Url
{
    const LOCAL_BASE = 'ec-local-attachments.s3.amazonaws.com';
    const DEV_BASE   = 'cheesycontent-attachments.s3.amazonaws.com';
    const LIVE_BASE  = 'easycontent-attachments.s3.amazonaws.com';

    const IMAGES_URL    = 0b1000; // 8
    const DOCUMENTS_URL = 0b0100; // 4
    const AUDIO_URL     = 0b0010; // 2
    const VIDEO_URL     = 0b0001; // 1
    const ANY_URL       = 0b1111; // 15

    const EXTENSIONS = [
        self::IMAGES_URL    => ['jpg', 'jpeg', 'png', 'gif', 'ico'],
        self::DOCUMENTS_URL => ['pdf', 'doc', 'docx', 'ppt', 'pptx', 'pps', 'ppsx', 'odt', 'xls', 'xlsx', 'psd'],
        self::AUDIO_URL     => ['mp3', 'm4a', 'ogg', 'wav'],
        self::VIDEO_URL     => ['mp4', 'm4v', 'mov', 'wmv', 'avi', 'mpg', 'ogv', '3gp', '3g2'],
    ];

    /** @var string $value */
    private $value;

    /** @var int $type */
    private $type;

    public function __construct($urlString, $type = self::ANY_URL)
    {
        $this->value = Str::removeExtraWhitespaces($urlString);
        $this->type = $type;
    }

    public function getValuableExtensionsArray()
    {
        $valuableExtensions = [];

        if (self::IMAGES_URL & $this->type) {
            $valuableExtensions = array_merge($valuableExtensions, self::EXTENSIONS[self::IMAGES_URL]);
        }

        if (self::DOCUMENTS_URL & $this->type) {
            $valuableExtensions = array_merge($valuableExtensions, self::EXTENSIONS[self::DOCUMENTS_URL]);
        }

        if (self::AUDIO_URL & $this->type) {
            $valuableExtensions = array_merge($valuableExtensions, self::EXTENSIONS[self::AUDIO_URL]);
        }

        if (self::VIDEO_URL & $this->type) {
            $valuableExtensions = array_merge($valuableExtensions, self::EXTENSIONS[self::VIDEO_URL]);
        }

        return $valuableExtensions;
    }

    public function getRegularExpression($base)
    {
        $escapedBase = preg_quote($base, '/');
        $valuableExtensions = $this->getValuableExtensionsArray();
        $extensionPattern = '(' . implode('|', $valuableExtensions) . ')';

        return "https?:\/\/{$escapedBase}.*{$extensionPattern}";
    }

    public function matchesRegularExpression($base)
    {
        $trimmedUrl = trim($this->value, '/');
        $urlRegex = $this->getRegularExpression($base);
        $pattern = "/^{$urlRegex}$/i";

        return 1 === preg_match($pattern, $trimmedUrl); // Url matches regular expression

        // TODO: Maybe process url on different languages
    }
}
