<?php

namespace PhpGangsters\EasyContent\HtmlHandler\DOM;
use PhpGangsters\EasyContent\HtmlHandler\DataTypes\Str;

final class Helper
{
    /**
     * Removes node but saves all child nodes on removed node's place
     */
    public static function removeChildOuter(\DOMElement $node)
    {
        if (!$node->parentNode) {
            // This is probably root node, we are not able to delete it
            return;
        }

        if (!$node->firstChild) {
            // Node has no children, simply remove
            $node->parentNode->removeChild($node);
            return;
        }

        $sibling = $node->firstChild;

        do {
            $next = $sibling->nextSibling;
            $node->parentNode->insertBefore($sibling, $node);
        } while ($sibling = $next);

        $node->parentNode->removeChild($node);
    }

    public static function isOldAnnotationsNode(\DOMElement $node)
    {
        return self::isAnnotationsNode($node, 'annotation');
    }

    public static function isNewAnnotationsNode(\DOMElement $node)
    {
        return self::isAnnotationsNode($node, 'assignment-comment');
    }

    private static function isAnnotationsNode(\DOMElement $node, $className)
    {
        if (!$classesString = $node->getAttribute('class')) {
            return false;
        }

        $classesString = Str::removeExtraWhitespaces($classesString);
        $classes = explode(' ', $classesString);

        return in_array($className, $classes, true);
    }
}
