<?php

namespace PhpGangsters\EasyContent\HtmlHandler\DOM;

use PhpGangsters\EasyContent\HtmlHandler\DataTypes\Url;
use PhpGangsters\EasyContent\HtmlHandler\FileList;
use PhpGangsters\EasyContent\HtmlHandler\Parsers\BaseParser;
use PhpGangsters\EasyContent\HtmlHandler\Parsers\SrcsetParser;

final class NodeAttribute
{
    const SRC    = 'src';
    const HREF   = 'href';
    const DATA   = 'data';
    const POSTER = 'poster';
    const SRCSET = 'srcset';


    /** @var string $name */
    private $name;

    /** @var string $value */
    private $value;

    /** @var FileList $filesList */
    private $filesList;

    /** @var int $expectedUrlType */
    private $expectedUrlType;

    public function __construct($name, $value, $expectedUrlType)
    {
        $this->name = $name;
        $this->value = $value;
        $this->expectedUrlType = $expectedUrlType;
        $this->filesList = new FileList();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getFileList()
    {
        return $this->filesList;
    }

    public function getExpectedUrlType()
    {
        return $this->expectedUrlType;
    }

    public function parse($base = Url::LIVE_BASE)
    {
        $parser = self::SRCSET === $this->name ? new SrcsetParser($base) : new BaseParser($base);
        $urls = $parser->parse($this);
        $this->filesList->addFilesFromArray($urls);

        return $this->filesList;
    }

    public function noUrlsFound()
    {
        return $this->filesList->isEmpty();
    }
}
