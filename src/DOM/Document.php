<?php

namespace PhpGangsters\EasyContent\HtmlHandler\DOM;

use PhpGangsters\EasyContent\HtmlHandler\DataTypes\Url;
use PhpGangsters\EasyContent\HtmlHandler\FileList;

final class Document extends \DOMDocument
{
    /** @var array|\LibXMLError[] $loadingErrors */
    private $loadingErrors = [];

    const NODE_NAMES_TO_PROCESS = [
        'a'      => ['href' => Url::ANY_URL],
        'area'   => ['href' => Url::ANY_URL],
        'embed'  => ['src' => Url::ANY_URL],
        'iframe' => ['src' => Url::ANY_URL],
        'video'  => [
            'src'    => Url::VIDEO_URL,
            'poster' => Url::IMAGES_URL
        ],
        'audio'  => ['src' => Url::AUDIO_URL],
        'img'    => [
            'src'    => Url::IMAGES_URL,
            'srcset' => Url::IMAGES_URL
        ],
        'source' => [
            'src'    => Url::AUDIO_URL | Url::VIDEO_URL, // For <audio> and <video> nodes
            'srcset' => Url::IMAGES_URL  // For <picture> only
        ],
        'track'  => ['src' => Url::ANY_URL],
        'object' => ['data' => Url::ANY_URL]
    ];

    /**
     * @var FileList $filesList
     */
    private $filesList;

    /**
     * @var array|Node[] $nodes
     */
    private $nodes = [];

    /**
     * @var int $processedNodes
     */
    private $processedNodes = 0;

    /**
     * @var float $parsingTime
     */
    private $parsingTime = 0;

    /**
     * @var string $urlBase
     */
    private $urlBase = '';

    public function __construct($version = '', $encoding = '', $urlBase = Url::LIVE_BASE)
    {
        parent::__construct($version, $encoding);

        $this->urlBase = $urlBase;

        // $this->formatOutput = true;
        // $this->substituteEntities = false;
        $this->preserveWhiteSpace = false;
        $this->recover = true;
        $this->xmlStandalone = true;

        $this->filesList = new FileList();
    }

    private function reset()
    {
        $this->processedNodes = 0;
        $this->parsingTime = 0;
        $this->filesList = new FileList();
        $this->nodes = [];
    }

    /**
     * @param string $source The HTML string
     * @param int $options This parameter will be ignored and overridden inside this method!
     * @return bool TRUE on success and FALSE otherwise
     */
    public function loadHTML($source, $options = 0)
    {
        $this->loadingErrors = [];

        libxml_use_internal_errors(true);

        // Converts HTML entities characters to entity name e.g. © will be converted to &copy;
        // It does not convert reserved characters such as: < > & ' "
        $convertedSource = mb_convert_encoding($source, 'HTML-ENTITIES', 'UTF-8');

        // LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NOENT | LIBXML_HTML_NOIMPLIED
        $options = LIBXML_HTML_NODEFDTD | LIBXML_NOXMLDECL | LIBXML_NSCLEAN | LIBXML_PARSEHUGE;

        $isSuccess = parent::loadHTML($convertedSource, $options);

        $this->loadingErrors = libxml_get_errors();

        libxml_clear_errors();

        return $isSuccess;
    }

    public function saveHTML($node = null)
    {
        $newLayout = parent::saveHTML($node);
        $newLayout = mb_convert_encoding($newLayout, 'UTF-8', 'HTML-ENTITIES');

        return trim($newLayout);
    }

    public function removeAnnotations()
    {
        $spans = $this->getElementsByTagName('span');
        $marks = $this->getElementsByTagName('mark');

        /** @var \DOMElement $spanNode*/
        foreach ($spans as $spanNode) {
            if (Helper::isOldAnnotationsNode($spanNode)) {
                Helper::removeChildOuter($spanNode);
            }
        }

        /** @var \DOMElement $markNode*/
        foreach ($marks as $markNode) {
            if (Helper::isNewAnnotationsNode($markNode)) {
                Helper::removeChildOuter($markNode);
            }
        }
    }

    public function getLoadingErrors()
    {
        return $this->loadingErrors;
    }

    public function getProcessedNodesNumber()
    {
        return $this->processedNodes;
    }

    public function getDOMParsingTime()
    {
        return $this->parsingTime;
    }

    public function getFilesList()
    {
        return $this->filesList;
    }

    public function getNodes()
    {
        return $this->nodes;
    }

    public function parse()
    {
        $startTime = microtime(true);

        $this->reset();
        $this->processNode($this->documentElement);

        $this->parsingTime = microtime(true) - $startTime;
    }

    private function processNode(\DOMElement $node)
    {
        ++$this->processedNodes;

        $nodeObject = new Node($node);

        // We need to process style attribute on every node in document
        $style = $node->getAttribute('style');

        if ($style) {
            // TODO: Process style attribute
        }

        // Using strtolower() just to be sure
        $nodeName = strtolower($node->nodeName);

        $nodeAttributesToProcess = array_key_exists($nodeName, self::NODE_NAMES_TO_PROCESS)
            ? self::NODE_NAMES_TO_PROCESS[$nodeName]
            : [];

        foreach ($nodeAttributesToProcess as $attributeName => $urlType) {
            if (!$attributeValue = $node->getAttribute($attributeName)) {
                continue;
            }

            $attribute = new NodeAttribute($attributeName, $attributeValue, $urlType);
            $detectedFiles = $attribute->parse($this->urlBase);

            if ($attribute->noUrlsFound()) {
                continue;
            }

            $this->filesList->merge($detectedFiles);
            $nodeObject->addAttribute($attribute);
        }

        if (!$nodeObject->isAttributesListEmpty()) {
            $this->nodes[] = $nodeObject;
        }

        if (!$node->hasChildNodes()) {
            return;
        }

        foreach ($node->childNodes as $childNode) {
            if ($childNode->nodeType !== XML_ELEMENT_NODE) {
                continue;
            }

            $this->processNode($childNode);
        }
    }
}
