<?php

namespace PhpGangsters\EasyContent\HtmlHandler\DOM;

final class NodeAttributeList implements \IteratorAggregate, \Countable, \ArrayAccess
{
    /** @var array|NodeAttribute[] $items */
    private $items = [];

    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public function count()
    {
        return count($this->items);
    }

    public function offsetExists($attributeName)
    {
        return $this->has($attributeName);
    }

    public function offsetGet($attributeName)
    {
        return $this->get($attributeName);
    }

    public function offsetSet($attributeName, $nodeAttribute)
    {
        $this->add($attributeName, $nodeAttribute);
    }

    public function offsetUnset($url) { }

    public function has($attributeName)
    {
        return isset($this->items[$attributeName]);
    }

    public function get($attributeName)
    {
        return $this->has($attributeName) ? $this->items[$attributeName] : null;
    }

    /**
     * @param string $attributeName
     * @param NodeAttribute $nodeAttribute
     */
    public function add($attributeName, NodeAttribute $nodeAttribute)
    {
        if ($this->has($attributeName)) {
            // TODO: Maybe add another behaviour
            return;
        }

        $this->items[$attributeName] = $nodeAttribute;
    }

    public function addAttribute(NodeAttribute $attribute)
    {
        $this->add($attribute->getName(), $attribute);
    }

    public function isEmpty()
    {
        return empty($this->items);
    }
}
