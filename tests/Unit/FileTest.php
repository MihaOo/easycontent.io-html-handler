<?php

namespace PhpGangsters\EasyContent\HtmlHandler\Tests\Unit;

use PhpGangsters\EasyContent\HtmlHandler\File;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    public function testGetsFileNameFromS3UrlProperly()
    {
        $url = 'https://ec-local-attachments.s3.amazonaws.com/1/22/39/-txlt3wpvwg1v7lqikalk4qzhnxhy11l/jpg.jpg';
        $fileName = File::getFileNameFromS3Url($url);

        $this->assertEquals('1_22_39_-txlt3wpvwg1v7lqikalk4qzhnxhy11l_jpg.jpg', $fileName);
    }
}
