<?php

namespace PhpGangsters\EasyContent\HtmlHandler\Parsers;

use PhpGangsters\EasyContent\HtmlHandler\DataTypes\Url;
use PhpGangsters\EasyContent\HtmlHandler\DOM\NodeAttribute;

class BaseParser
{
    /** @var string $s3UrlBase */
    protected $s3UrlBase;

    public function __construct($base)
    {
        $this->s3UrlBase = $base;
    }

    /**
     * @param NodeAttribute $nodeAttribute
     * @return array|string[]
     */
    public function parse(NodeAttribute $nodeAttribute)
    {
        $matches = (new Url($nodeAttribute->getValue(), $nodeAttribute->getExpectedUrlType()))
            ->matchesRegularExpression($this->s3UrlBase);

        return $matches ? [$nodeAttribute->getValue()] : [];
    }
}
